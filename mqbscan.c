/*
  Copyright (c) 2017, Mans Rullgard <mans@mansr.com>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <unistd.h>
#include <sndfile.h>

#include "bits.h"
#include "sfbits.h"
#include "mqa-common.h"
#include "mqbcrc.h"

#define MQB_MARKER	0x00048c49e0ab2c

struct bitreader br;
struct bitreader fb;

static unsigned sample_pos;
static jmp_buf eofjmp;

static int verbosity = 1;
static int packet_type = -1;
static int single;

static const struct bitfield frame_00[] = {
	{ 8,	BF_RET,		"size" },
	{ 32,	BF_HEX,		"magic" },
	{ 0 },
};

static const struct bitfield frame_01[] = {
	{ 8,	BF_RET,		"size" },
	{ 5,	0,		"resampling_filter" },
	{ 2,	0,		"noise_shaping_filter" },
	{ 2,	0,		"bitdepth",	mqb_bitdepths },
	{ 1,	0,		"unknown" },
	{ 10,	BF_SIGNED,	"replay_gain" },
	{ 5,	BF_HEX,		"orig_rate",	mqa_rates },
	{ 2,	0,		"unknown" },
	{ 1,	0,		"unknown" },
	{ 1,	0,		"swizzle" },
	{ 3,	0,		"pad" },
	{ 0 },
};

static const struct bitfield frame_02[] = {
	{ 8,	BF_RET,		"size" },
	{ 16,	BF_SIGNED,	"gain" },
	{ 0 },
};

static const struct bitfield frame_04[] = {
	{ 8,	BF_RET,		"size" },
	{ 8,	0,		"type" },
	{ 0 },
};

static const struct frame frame_types[256] = {
	[0] = {
		.name	= "magic",
		.fields	= frame_00,
	},
	[1] = {
		.name	= "stream_params",
		.fields	= frame_01,
	},
	[2] = {
		.name	= "gain",
		.fields	= frame_02,
	},
	[4] = {
		.name	= "metadata",
		.fields	= frame_04,
	},
};

static uint32_t crc;

static void fill_bits_mqb(struct bitreader *b, int n)
{
	int bits;

	while (b->bitend < b->bitpos + n) {
		bits = get_ubits(&br, 8);
		put_byte(b, bits);
	}
}

static int print_mqb_frame(struct bitreader *b)
{
	unsigned spos = sample_pos;
	int type;
	int size;
	int epos;
	int cc;
	int cr;

	crc = 0;

	type = get_ubits(b, 8);

	if (packet_type >= 0 && type != packet_type)
		print_verbosity(0);

	size = print_frame(b, &frame_types[type], type, spos);
	epos = spos + 8 + size * 8;

	while (sample_pos < epos)
		get_ubits(b, 8);

	cc = mqb_crc_end(crc);
	cr = print_field(b, 8, BF_HEX, "crc", NULL);

	print_verbosity(verbosity);

	if (cc != cr)
		printf("%08x: checksum error\n", sample_pos);

	return type;
}

static void scan_mqb(struct bitreader *b)
{
	for (;;) {
		int type = print_mqb_frame(b);

		if (single && (packet_type < 0 || type == packet_type))
			break;
	}
}

static void get_bits_cb(uint64_t bits, int n)
{
	crc = mqb_crc(crc, bits);
	sample_pos += n;
}

static void eof_cb(void)
{
	longjmp(eofjmp, 1);
}

int scan_file(const char *name, int start)
{
	SNDFILE *file;
	SF_INFO fmt;

	memset(&fmt, 0, sizeof(fmt));
	file = sf_open(name, SFM_READ, &fmt);
	if (!file) {
		fprintf(stderr, "%s: %s\n", name, sf_strerror(NULL));
		return -1;
	}

	if (fmt.channels != 2) {
		fprintf(stderr, "Only 2 channels supported\n");
		return -1;
	}

	print_verbosity(verbosity);

	sf_seek(file, start, SEEK_SET);
	sample_pos = start;

	if (!setjmp(eofjmp)) {
		init_bits_sf(&br, file, 0);
		br.msb = 1;
		br.get_bits_cb = get_bits_cb;
		br.eof_cb = eof_cb;

		init_bits(&fb);
		fb.fill_bits = fill_bits_mqb;

		if (!find_sync(&br, MQB_MARKER, 56, 1024))
			scan_mqb(&fb);
	}

	sf_close(file);

	return 0;
}

int main(int argc, char **argv)
{
	int start = 0;
	int opt;
	int i;

	while ((opt = getopt(argc, argv, "1p:s:v")) != -1) {
		switch (opt) {
		case '1':
			if (single)
				start++;
			single = 1;
			if (packet_type < 0)
				packet_type = 1;
			break;
		case 'p':
			packet_type = strtol(optarg, NULL, 0);
			break;
		case 's':
			start = strtol(optarg, NULL, 0);
			break;
		case 'v':
			verbosity++;
			break;
		default:
			return 1;
		}
	}

	argc -= optind;
	argv += optind;

	if (argc < 1)
		return 1;

	for (i = 0; i < argc; i++) {
		if (argc > 1)
			printf("%s\n", argv[i]);

		scan_file(argv[i], start);
	}

	return 0;
}
