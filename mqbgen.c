/*
  Copyright (c) 2017, Mans Rullgard <mans@mansr.com>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sndfile.h>

#include "mqbcrc.h"

struct mqb_params {
	unsigned resampling_filter;
	unsigned noise_shaping_filter;
	unsigned bitdepth;
	unsigned unknown_1;
	int replay_gain;
	unsigned orig_rate;
	unsigned unknown_2;
	unsigned unknown_3;
	unsigned swizzle;
};

#define BUF_SIZE 4096

static uint8_t *mqb_write_frame(uint8_t *buf, int type,
				const uint8_t *data, int len)
{
	uint8_t *p = buf;

	*p++ = type;
	*p++ = len;

	memcpy(p, data, len);
	p += len;

	*p++ = mqb_crc_buf(buf, len + 2);

	return p;
}

static uint8_t *mqb_write_magic(uint8_t *buf)
{
	static const uint8_t mqb_magic[4] = { 0x8c, 0x49, 0xe0, 0xab };

	return mqb_write_frame(buf, 0, mqb_magic, 4);
}

static uint8_t *mqb_write_params(uint8_t *buf, struct mqb_params *mp)
{
	uint8_t pb[4];
	uint32_t p;

	p = (mp->resampling_filter & 0x1f) |
		(mp->noise_shaping_filter & 3) << 5 |
		(mp->bitdepth & 3) << 7 |
		(mp->unknown_1 & 1) << 9 |
		(mp->replay_gain & 0x3ff) << 10 |
		(mp->orig_rate & 0x1f) << 20 |
		(mp->unknown_2 & 3) << 25 |
		(mp->unknown_3 & 1) << 27 |
		(mp->swizzle & 1) << 28;

	pb[0] = p;
	pb[1] = p >> 8;
	pb[2] = p >> 16;
	pb[3] = p >> 24;

	return mqb_write_frame(buf, 1, pb, 4);
}

static uint8_t *mqb_write_gain(uint8_t *buf, int gain)
{
	uint8_t g[2] = { gain, gain >> 8 };

	return mqb_write_frame(buf, 2, g, 2);
}

static void error(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	exit(1);
}

static int32_t swizzle(int32_t v)
{
	return (v >> 8 & 0xff0) | (v << 8 & 0xff000) | (v & ~0xffff0);
}

static int32_t rng_next(void)
{
	static uint32_t rng = 0xdeadbeef;

	return rng = 1664525 * rng + 1013904223;
}

static void mqb_write_bit(int32_t *l, int32_t *r, int bit)
{
	int rv = rng_next() & 1;
	int c = ((*l ^ *r) & 1) ^ bit;

	if (rv)
		*l ^= c;
	else
		*r ^= c;
}

int main(int argc, char **argv)
{
	const char *opts = "f:n:b:G:r:sg:";
	struct mqb_params mp;
	SNDFILE *ifile;
	SNDFILE *ofile;
	SF_INFO ifmt;
	SF_INFO ofmt;
	int32_t *ibuf;
	int32_t *obuf;
	uint8_t mqb[32];
	uint8_t *mqb_end;
	int mqb_len;
	int mqb_pos;
	int mqb_bit;
	int rate_idx;
	int rate_mul;
	int rate = 2;
	int gain = 0;
	int opt;

	memset(&mp, 0, sizeof(mp));

	mp.resampling_filter = 4;
	mp.noise_shaping_filter = 0;
	mp.bitdepth = 2;
	mp.unknown_2 = 3;

	while ((opt = getopt(argc, argv, opts)) != -1) {
		switch (opt) {
		case 'f':
			mp.resampling_filter = strtol(optarg, NULL, 0);
			break;
		case 'n':
			mp.noise_shaping_filter = strtol(optarg, NULL, 0);
			break;
		case 'b':
			mp.bitdepth = strtol(optarg, NULL, 0);
			break;
		case 'G':
			mp.replay_gain = strtol(optarg, NULL, 0);
			break;
		case 'r':
			rate = strtol(optarg, NULL, 0);
			break;
		case 's':
			mp.swizzle = 1;
			break;
		case 'g':
			gain = strtol(optarg, NULL, 0);
			break;
		default:
			return 1;
		}
	}

	argc -= optind;
	argv += optind;

	if (argc < 2)
		return 1;

	memset(&ifmt, 0, sizeof(ifmt));

	ifile = sf_open(argv[0], SFM_READ, &ifmt);
	if (!ifile)
		error("%s: %s\n", argv[0], sf_strerror(NULL));

	if (ifmt.channels > 2)
		error("unsupported channel count %d\n", ifmt.channels);

	if (rate < 8) {
		rate_mul = rate;
		rate = ifmt.samplerate;
	} else {
		rate_mul = 0;

		while (rate > 64000) {
			rate_mul++;
			rate >>= 1;
		}
	}

	if (!(rate % 44100))
		rate_idx = 0;
	else if (!(rate % 48000))
		rate_idx = 1;
	else if (!(rate % 64000))
		rate_idx = 2;
	else
		error("unsupported sample rate %d\n", rate);

	mp.orig_rate = rate_idx << 3 | rate_mul;

	switch (mp.bitdepth) {
	case 20:
		mp.bitdepth = 0;
		break;
	case 18:
		mp.bitdepth = 1;
		break;
	case 16:
		mp.bitdepth = 2;
		break;
	case 15:
		mp.bitdepth = 3;
		break;
	default:
		if (mp.bitdepth > 3)
			error("unsupported bit depth %d\n", mp.bitdepth);
		break;
	}

	ibuf = malloc(BUF_SIZE * ifmt.channels * sizeof(*ibuf));
	if (!ibuf)
		error("malloc: %s\n", strerror(errno));

	memset(&ofmt, 0, sizeof(ofmt));
	ofmt.samplerate = ifmt.samplerate;
	ofmt.channels = 2;
	ofmt.format = SF_FORMAT_WAV | SF_FORMAT_PCM_24;

	obuf = malloc(BUF_SIZE * ofmt.channels * sizeof(*obuf));
	if (!obuf)
		error("malloc: %s\n", strerror(errno));

	ofile = sf_open(argv[1], SFM_WRITE, &ofmt);
	if (!ofile)
		error("%s: %s\n", argv[1], sf_strerror(NULL));

	mqb_end = mqb_write_magic(mqb);
	mqb_end = mqb_write_params(mqb_end, &mp);
	mqb_end = mqb_write_gain(mqb_end, gain);

	mqb_len = mqb_end - mqb;
	mqb_pos = 0;
	mqb_bit = 7;

	for (;;) {
		int frames = sf_readf_int(ifile, ibuf, BUF_SIZE);
		int i;

		if (!frames)
			break;

		for (i = 0; i < frames; i++) {
			int bit = mqb[mqb_pos] >> mqb_bit & 1;
			int32_t l, r;

			if (ifmt.channels == 1) {
				l = r = ibuf[i] >> 8;
			} else {
				l = ibuf[2 * i] >> 8;
				r = ibuf[2 * i + 1] >> 8;
			}

			if (mqb_bit-- == 0) {
				if (++mqb_pos == mqb_len)
					mqb_pos = 0;
				mqb_bit = 7;
			}

			if (mp.swizzle) {
				l = swizzle(l);
				r = swizzle(r);
			}

			mqb_write_bit(&l, &r, bit);

			obuf[2 * i] = l << 8;
			obuf[2 * i + 1] = r << 8;

		}

		sf_writef_int(ofile, obuf, frames);
	}

	sf_close(ifile);
	sf_close(ofile);

	free(ibuf);
	free(obuf);

	return 0;
}
