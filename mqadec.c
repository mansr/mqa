/*
  Copyright (c) 2017, Mans Rullgard <mans@mansr.com>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sndfile.h>

#include "bluos_ssc.h"

#define BUF_SIZE 4096

static int bluos_api;

static SNDFILE *infile;
static SF_INFO infmt;
static uint8_t buf[BUF_SIZE];
static int buf_pos;
static int buf_end;

static int samples_in;
static int samples_out;

static const char *outname;
static SNDFILE *outfile;
static SF_INFO outfmt;

static int get_samples(int frame_size, uint8_t **samples, int eof, int *end)
{
	int size;

	if (buf_pos > 0) {
		size = buf_end - buf_pos;
		memmove(buf, buf + buf_pos, size);
		buf_pos = 0;
		buf_end = size;
	}

	if (infile && buf_end < BUF_SIZE) {
		int frames = (BUF_SIZE - buf_end) / frame_size;

		frames = sf_readf_int(infile, (int *)&buf[buf_end], frames);
		if (!frames) {
			sf_close(infile);
			infile = NULL;
		}

		buf_end += frames * frame_size;
	}

	size = buf_end - buf_pos;
	size -= size % frame_size;

	if (end) {
		if (!size) {
			*end = 1;
			return 0;
		}

		*end = 0;
	}

	*samples = &buf[buf_pos];

	if (bluos_api > 0)
		size /= frame_size;

	return size;
}

static void consume(int len)
{
	buf_pos += len;
	samples_in += len / (4 * infmt.channels);
}

static size_t write_samples(void *p, void *buf, size_t len)
{
	int32_t *s = buf;
	size_t ret;
	int i;

	if (bluos_api < 1)
		len /= 8;

	samples_out += len;

	if (!outfile) {
		int rs;

		for (rs = 0; samples_out > samples_in << rs; rs++);

		outfmt.samplerate = infmt.samplerate << rs;
		outfmt.channels = infmt.channels;
		outfmt.format = SF_FORMAT_WAV | SF_FORMAT_PCM_24;

		outfile = sf_open(outname, SFM_WRITE, &outfmt);
		if (!outfile) {
			fprintf(stderr, "%s: %s\n", outname, sf_strerror(NULL));
			exit(1);
		}
	}

	for (i = 0; i < len * 2; i++)
		s[i] <<= 8;

	ret = sf_writef_int(outfile, s, len);

	if (bluos_api < 1)
		ret *= 8;

	return ret;
}

static int write_size(void *p)
{
	return bluos_api < 1 ? BUF_SIZE * 8 : BUF_SIZE;
}

int main(int argc, char **argv)
{
	struct ssc_decode *sscd;
	const char *lib = NULL;
	int channels = 2;
	int rate1 = 0;
	int rate2 = 0;
	int bits = 32;
	int options = 0;
	int mqa = 0;
	int opt;

	while ((opt = getopt(argc, argv, "L:o:r:")) != -1) {
		switch (opt) {
		case 'L':
			lib = optarg;
			break;
		case 'o':
			options = strtol(optarg, NULL, 0);
			break;
		case 'r':
			rate2 = strtol(optarg, NULL, 0);
			break;
		default:
			return 1;
		}
	}

	argc -= optind;
	argv += optind;

	if (argc < 2) {
		fprintf(stderr, "Input and output filenames must be given\n");
		return 1;
	}

	memset(&infmt, 0, sizeof(infmt));
	infile = sf_open(argv[0], SFM_READ, &infmt);
	if (!infile) {
		fprintf(stderr, "%s: %s\n", argv[0], sf_strerror(NULL));
		return 1;
	}

	rate1 = infmt.samplerate;
	channels = infmt.channels;
	outname = argv[1];

	if (ssc_init(lib))
		return 1;

	bluos_api = ssc_decode_open(&sscd, channels, rate1, bits, rate2,
				    get_samples, consume, write_samples,
				    write_size, &outfile, options);

	if (bluos_api < 0)
		return 1;

	while (ssc_decode_read(sscd) > 0) {
		if (!mqa) {
			const char *s = ssc_decode_status(sscd);
			if (s[0] != '0') {
				fprintf(stderr, "%s\n", s);
				mqa = 1;
			}
		}
	}

	ssc_decode_close(sscd);

	sf_close(outfile);

	return 0;
}
