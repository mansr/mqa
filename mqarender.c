/*
  Copyright (c) 2017, Mans Rullgard <mans@mansr.com>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sndfile.h>
#include <alsa/asoundlib.h>
#include <alsa/pcm_rate.h>

#include "bluos_ssc.h"

#define BUF_SIZE 4096

static void setup_areas(snd_pcm_channel_area_t *a, void *buf,
			int channels, int bits)
{
	int i;

	for (i = 0; i < channels; i++) {
		a[i].addr = buf;
		a[i].first = i * bits;
		a[i].step = channels * bits;
	}
}

int main(int argc, char **argv)
{
	struct ssc_render *rend = NULL;
	const char *lib = NULL;
	SNDFILE *infile;
	SF_INFO infmt;
	SNDFILE *outfile;
	SF_INFO outfmt;
	snd_pcm_rate_info_t ri;
	snd_pcm_channel_area_t in_areas[2];
	snd_pcm_channel_area_t out_areas[2];
	int32_t *inbuf;
	int32_t *outbuf;
	int outbuf_size;
	int channels = 2;
	int rate1 = 0;
	int rate2 = 0;
	int ratio;
	int opt;

	while ((opt = getopt(argc, argv, "L:r:")) != -1) {
		switch (opt) {
		case 'L':
			lib = optarg;
			break;
		case 'r':
			rate2 = strtol(optarg, NULL, 0);
			break;
		default:
			return 1;
		}
	}

	argc -= optind;
	argv += optind;

	if (argc < 2) {
		fprintf(stderr, "Input and output filenames must be given\n");
		return 1;
	}

	memset(&infmt, 0, sizeof(infmt));
	infile = sf_open(argv[0], SFM_READ, &infmt);
	if (!infile) {
		fprintf(stderr, "%s: %s\n", argv[0], sf_strerror(NULL));
		return 1;
	}

	rate1 = infmt.samplerate;
	channels = infmt.channels;

	if (!rate1) {
		fprintf(stderr, "unknown sample rate\n");
		return 1;
	}

	if (channels != 2) {
		fprintf(stderr, "Only 2 channels supported\n");
		return 1;
	}

	if (!rate2)
		rate2 = 2 * rate1;
	else if (rate2 <= 32)
		rate2 = rate1 * rate2;

	if (rate2 % rate1) {
		fprintf(stderr, "Output rate must be multiple of input rate\n");
		return 1;
	}

	ratio = rate2 / rate1;

	memset(&outfmt, 0, sizeof(outfmt));
	outfmt.samplerate = rate2;
	outfmt.channels = channels;
	outfmt.format = SF_FORMAT_WAV | SF_FORMAT_PCM_24;

	outfile = sf_open(argv[1], SFM_WRITE, &outfmt);
	if (!outfile) {
		fprintf(stderr, "%s: %s\n", argv[1], sf_strerror(NULL));
		return 1;
	}

	inbuf = malloc(8 * BUF_SIZE);
	if (!inbuf) {
		perror("malloc");
		return 1;
	}

	outbuf_size = BUF_SIZE * ratio;
	outbuf = malloc(8 * outbuf_size);
	if (!outbuf) {
		perror("malloc");
		return 1;
	}

	setup_areas(in_areas, inbuf, channels, 32);
	setup_areas(out_areas, outbuf, channels, 32);

	ri.channels = channels;

	ri.in.format = SND_PCM_FORMAT_S24;
	ri.in.rate = rate1;
	ri.in.buffer_size = BUF_SIZE;
	ri.in.period_size = BUF_SIZE;

	ri.out.format = SND_PCM_FORMAT_S24;
	ri.out.rate = rate2;
	ri.out.buffer_size = outbuf_size;
	ri.out.period_size = outbuf_size;

	if (ssc_init(lib))
		return 1;

	if (ssc_render_init(&rend, &ri, 0, NULL) < 0) {
		fprintf(stderr, "ssc_render_init failed\n");
		return 1;
	}

	for (;;) {
		int frames = sf_readf_int(infile, inbuf, BUF_SIZE);
		int i;

		if (!frames)
			break;

		for (i = 0; i < frames * channels; i++)
			inbuf[i] >>= 8;

		ssc_render(rend, out_areas, 0, frames * ratio,
			   in_areas, 0, frames, 0, channels,
			   SND_PCM_FORMAT_S24);

		for (i = 0; i < frames * channels * ratio; i++)
			outbuf[i] <<= 8;

		sf_writef_int(outfile, outbuf, frames * ratio);
	}

	ssc_render_free(rend);
	ssc_render_close(rend);

	sf_close(infile);
	sf_close(outfile);

	return 0;
}
